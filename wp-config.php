<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'git' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ';!3.Ea+,O?,5/=27XGu#0oX5D$c Bi1O#?vtG&s;l]HpQwl7[T(H].PT(]mii>*~' );
define( 'SECURE_AUTH_KEY',  'x~{w&Z)u1[>34y}fq_L|W?4j=&8E(O~RJT>a<v*|+;,73CoAl0;YyK/.wp_!XY(#' );
define( 'LOGGED_IN_KEY',    ']R|<Uw%vV7&Lp+shU?t2hAvOvE5lT^Pve=c-UH@*E+Q8++$7mOCon7hy2ODbVue,' );
define( 'NONCE_KEY',        '|tp[K+xt80|xEtrZ.9l{2CpM,q_yg:{X!kkUM[NGys#v- ~e. G{LUK~t7&zmTau' );
define( 'AUTH_SALT',        'yfZ=rp`Bfd^mMtzLKjds#/[h*djc`8OzkgbkX3M]{08#x)li{7_Y+bJW:|m+jMq`' );
define( 'SECURE_AUTH_SALT', '[Q?n&MOb&Ofw_W;?Uog7 $#yjT,(eEe|dEf_5ols-XF@z%yh 6hp-bXp<3KFm0?+' );
define( 'LOGGED_IN_SALT',   'v#077.UL{^}(|.-wcx36ofk3O-~i987Wi%#~Ov>efA@X*H%F/+cZ:Mu&,hGaE3}{' );
define( 'NONCE_SALT',       'azfV&.{OiIdq>[h|H@ob]0/7hSU$ R/f3#!]%bn`!cvl-v$y0ly5hqb>V!,V@j>n' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
